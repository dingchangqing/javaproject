### Future的使用

> Future是一个异步任务的结果，它由ExecutorService.submit() 方法返回，可以使用get()方法获取任务的执行结果，也可以使用cancel取消计算。


#### get()方法会进行阻塞等待

	public class TestFuture {
    static  ExecutorService executorService = null;
    static ArchiveSeacher seacher = null;

    public static void main(String[] args) {
        executorService = Executors.newFixedThreadPool(3);
        seacher = new ArchiveSeacher();

        searchMethod("ding1");
        searchMethod("ding2");
        searchMethod("ding3");


        executorService.shutdown();
    }

    private static void searchMethod(final String target) {
        Future<String> stringFuture = executorService.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return seacher.search(target);
            }
        });

        doSomething();

        try {
			// 阻塞等待获取任务结果
            String result = stringFuture.get();
            System.out.println("finally search result:"+result);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private static void doSomething() {
	//        System.out.println("start doSomething...");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
	//        System.out.println("end doSomething...");
    }

    static class ArchiveSeacher implements IArchiveSearcher{

        private Random random = new Random();
        @Override
        public String search(String target) {
            String resultTime = "";
            try {
                int time = random.nextInt(10000);
                System.out.println("now is search target:"+target+"\t time:"+time);
                Thread.sleep(time);
                resultTime = "target search time:"+time;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return resultTime;
        }
    }
	}

### FutureTask 

> 可取消的异步计算。可使用 FutureTask 包装 Callable 或 Runnable 对象。因为 FutureTask 实现了 Runnable，所以可将 FutureTask 提交给 Executor 执行。 

示例：

	public class TestFutureTask {
    static ExecutorService executorService = null;

    public static void main(String[] args) {

        executorService = Executors.newFixedThreadPool(2);

        final FutureTask<String> futureTask = new FutureTask<String>(new Callable<String>() {
            @Override
            public String call() throws Exception {
                System.out.println("prepare execute task...");
                Thread.sleep(100000);
                String result = "task execute over...";
                System.out.println("end execute task...");
                return result;
            }
        }){
            /**
             * 此方法是父类留给子类可选择性的扩展方法，此方法不管是任务正常执行还是被取消时，都会被调用
             */
            @Override
            protected void done() {

                System.out.println("FutureTask done...");
            }
        };

        executorService.execute(futureTask);

        new Thread(){
            @Override
            public void run() {
                try {
                    Thread.sleep(new Random().nextInt(2000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // 取消正在执行的任务
                futureTask.cancel(true);
                System.out.println("futureTask is cancel...");
            }
        }.start();

        try {
            System.out.println("futureTask.get():" + futureTask.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (CancellationException e) {     // 任务取消后会抛次异常
            System.out.println("task is cancel...");
        }
        executorService.shutdown();
    }
	}

* get()方法

	> 和Future的get()方法一样，阻塞性的等待任务的执行结果。

* cancel(boolean mayInterruptIfRunning)

	> 取消任务，根据传入的mayInterruptIfRunning决定是否取消正在执行的任务

* done()

	> 此方法是protected 类型的，所以子类（实现类）可以通过实现该方法来监听任务的执行情况，因为该方法无论在任务正常执行结束还是中途取消，最后都会执行该方法，此方法在FutureTaks中时一个空的实现，就是留给子类进行扩展的。

以下是Android中AsyncTask中使用到的FutureTask的用法

	mFuture = new FutureTask<Result>(mWorker) {
            @Override
            protected void done() {
                try {
                    postResultIfNotInvoked(get());
                } catch (InterruptedException e) {	//被中断
                    android.util.Log.w(LOG_TAG, e);
                } catch (ExecutionException e) {	
                    throw new RuntimeException("An error occured while executing doInBackground()",
                            e.getCause());
                } catch (CancellationException e) {	// 被取消
                    postResultIfNotInvoked(null);
                }
            }
        };

	private void postResultIfNotInvoked(Result result) {
        final boolean wasTaskInvoked = mTaskInvoked.get();
        if (!wasTaskInvoked) {
            postResult(result);
        }
    }

在done()方法中，根据最终FutureTask的状态，来决定如何处理程序。

