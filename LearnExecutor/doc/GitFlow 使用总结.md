### GitFlow 使用总结

> git flow和git不是同一个概念，git flow是对git的封装或包装，可以更好去对git进行管理。

#### 安装GitFlow 

> 在安装完github fow window以后，并没有gitflow功能，这个需要再进行安装。

gitflow 的安装在Github上，地址：https://github.com/nvie/gitflow/wiki/Windows

* 第一步

> 这篇文章提到，需要将 getopt.exe ,  libintl3.dll  and  libiconv2.dll  三个文件copy到本地\GitHub\Portab~1\bin目录下，其中Protab~1是一个很长的目录，在GitHub目录下可以看到。

* 第二步

> 运行Git Shell ，进入本地的GitHub目录下，执行以下语句：

	C:\GitHub> git clone --recursive git://github.com/nvie/gitflow.git

这个命令就去下载gitflow了。

* 第三步

> gitflow下载完成后，在GitHub目录下生成一个gitflow的目录，进入这个目录：

	cd gitflow

* 第四步

> 进入gitflow后，默认显示：本地路径/GitHub\gitflow [develop]> 

	contrib\msysgit-install.cmd "%LOCALAPPDATA%\GitHub\Portab~1"

执行上面的命令行，需要替换其中的%LOCALAPPDATA%为自己的本地路径。

* 第五步

> 验证，执行下面的语句验证gitflow是否正常安装。

	C:\GitHub> git flow help 

#### gitflow的介绍和简单使用可以参看下面文章

	http://www.berlinix.com/it/gitflow.php

### git用命令总结

#### 查看已有分支

	git branch

#### 查看分支状态

	git status

#### 切换分支

	git checkout develop[分支名]

#### push到远程分支

	git push origin develop[远程分支名称]

#### 删除某个分支

	git branch -d test[要删除的分支名称]

如果在删除test分支时，test分支上存在未提交或者合并的改动，此时删除分支操作失败。

如果确实确定不需要test分支，且不想进行合并，则可以使用大写-D来进行强制删除。

#### 合并分支

	git merge develop[要合并的那个分支名称]


### gitflow常用命令

##### 初始化

	git flow init

执行上面命令，一直回车会默认初始化以下分支：

* master分支（1个）
* develop分支（1个）
* feature分支。同时存在多个。
* release分支。同一时间只有1个，生命周期很短，只是为了发布。
* hotfix分支。同一时间只有1个。生命周期较短，用了修复bug或小粒度修改发布。

如果出现下面错误：

	$ git flow init
	fatal: Working tree contains unstaged changes. Aborting.

说明目录下有修改未commit，需要commit后再次进行gitflow初始化。

#### 创建完成特性分支
	
	// 创建一个特性分支：myfeature
	$ git flow feature start myfeature

	// 查看分支
	$ git branch
	  develop
	* feature/myfeature
	  master
创建一个新的特性分支以后，默认切到当前特性分支。

当在该特性分支上完成开发后，执行完成特性分支的命令：

	$ git flow feature finish myfeature

	// 输出下列提示
	Summary of actions:
	- The feature branch 'feature/myfeature' was merged into 'develop'
	- Feature branch 'feature/myfeature' has been removed
	- You are now on branch 'develop'

正如这条命令的总结所言，git flow为我们做了3件事：

* 把feature/myfeature合并到了develop。
* 删除了feature/myfeature分支。
* 切换回develop分支。

当前又切回到develop分支上了。


##### release发布分支

> 在确定要发布一个版本的时候，可以创建一个release分支，这个分支包含当前版本无bug可发布的代码分支。该分支是从develop分支拉出的一个新的分支。

	$ git flow release start v1.5

上面的命令就创建了v1.5的release分支。

一旦最终确定代码无误，可以发布，可以使用finish命令。

	git flow release finish v1.5

在finish时会将release分支合并到develop和master分支上，并删除release分支，切回develop分支。此时在提交的时候，会让打一个release的Tag，根据当前的release的版本打一个tag标记。

#### 将本地Tag发布到远程服务器

	git push origin [tag名]

#### 删除远程分支

> 在本地某个分支被finish以后，本地会自动删除该分支，但是远程服务器如果之前publish这个分支，则服务器端该分支还存在，所以需要手动删除该分支。

	git push origin :【远程分支名称】

origin后面的空格和远程分支名称前面的:不能少。



	

