package cn.qing.executor.serial;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.Executor;

/**
 * User: qing
 * Date: 2015-05-16 14:38
 * Desc: 序列化的线程Executor,顺序的执行Runnable任务
 */
public class SerialExecutor implements Executor {

    final Queue<Runnable> tasks = new ArrayDeque<>();
    Executor executor;
    Runnable active;

    public SerialExecutor(Executor executor) {
        this.executor = executor;
    }


    @Override
    public synchronized void execute(final Runnable r) {
        final String currentName;
        if (r instanceof PrintRunnable) {
            currentName = ((PrintRunnable)r).getName();
        }
        else
            currentName = "";
//        System.out.println("tasks.offer start:"+currentName);
        tasks.offer(new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("start execute:"+currentName);
                    r.run();
                    System.out.println("end execute:" + currentName);
                } finally {
                    scheduleNext();
                }
            }
        });
//        System.out.println("tasks.offer end:" + currentName);
        if (active == null) {
            scheduleNext();
        }
    }

    private synchronized void scheduleNext() {
        if ((active = tasks.poll()) != null) {
            executor.execute(active);
        }
    }
}
