package cn.qing.executor.future;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * User: qing
 * Date: 2015-05-16 16:23
 * Desc:
 */
public class TestFuture {
    static  ExecutorService executorService = null;
    static ArchiveSeacher seacher = null;

    public static void main(String[] args) {
        executorService = Executors.newFixedThreadPool(3);
        seacher = new ArchiveSeacher();

        searchMethod("ding1");
        searchMethod("ding2");
        searchMethod("ding3");


        executorService.shutdown();
    }

    private static void searchMethod(final String target) {
        Future<String> stringFuture = executorService.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return seacher.search(target);
            }
        });

        doSomething();

        try {
            String result = stringFuture.get();
            System.out.println("finally search result:"+result);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private static void doSomething() {
//        System.out.println("start doSomething...");
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        System.out.println("end doSomething...");
    }

    static class ArchiveSeacher implements IArchiveSearcher{

        private Random random = new Random();
        @Override
        public String search(String target) {
            String resultTime = "";
            try {
                int time = random.nextInt(10000);
                System.out.println("now is search target:"+target+"\t time:"+time);
                Thread.sleep(time);
                resultTime = "target search time:"+time;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return resultTime;
        }
    }
}
