package cn.qing.executor.future;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

/**
 * User: qing
 * Date: 2015-05-16 17:14
 * Desc: 使用FutureTask来获取任务执行的结果，可以直接将FutureTask传递给Executor去执行
 */
public class TestFutureTask {
    static ExecutorService executorService = null;

    public static void main(String[] args) {

        executorService = Executors.newFixedThreadPool(2);

        final FutureTask<String> futureTask = new FutureTask<String>(new Callable<String>() {
            @Override
            public String call() throws Exception {
                System.out.println("prepare execute task...");
                Thread.sleep(100000);
                String result = "task execute over...";
                System.out.println("end execute task...");
                return result;
            }
        }){
            /**
             * 此方法是父类留给子类可选择性的扩展方法，此方法不管是任务正常执行还是被取消时，都会被调用
             */
            @Override
            protected void done() {

                System.out.println("FutureTask done...");
            }
        };

        executorService.execute(futureTask);

        new Thread(){
            @Override
            public void run() {
                try {
                    Thread.sleep(new Random().nextInt(2000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // 取消正在执行的任务
                futureTask.cancel(true);
                System.out.println("futureTask is cancel...");
            }
        }.start();

        try {
            System.out.println("futureTask.get():" + futureTask.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (CancellationException e) {     // 任务取消后会抛次异常
            System.out.println("task is cancel...");
        }
        executorService.shutdown();
    }
}
