package cn.qing.executor.future;

/**
 * User: qing
 * Date: 2015-05-16 16:25
 * Desc: 定义一个档案搜索的接口
 */
public interface IArchiveSearcher {

    /**
     *
     * @param target 搜索的名称
     * @return 搜索到的结果
     */
    public String search(String target);
}
