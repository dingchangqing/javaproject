package cn.qing.executor.schedule;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * User: qing
 * Date: 2015-05-16 18:05
 * Desc: 使用ScheduleExecutorService来执行定时任务的调度
 */
public class TestScheduleExecutor {

    private static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    public static void main(String[] args) {
        /*scheduleByCallable();

        scheduleByRunnable();*/

        scheduleAtFixedRate();

        scheduler.shutdown();

    }

    /**
     * 使用Callable进行一次调度
     */
    public static void scheduleByCallable() {
        Callable<String> callable = new Callable<String>() {
            @Override
            public String call() throws Exception {
                System.out.println("execute task...");
                return "executed";
            }
        };

        // 延迟10s进行任务调度，这种方式，只调度了一次
        ScheduledFuture<String> scheduledFuture = scheduler.schedule(callable, 10, TimeUnit.SECONDS);
        try {
            String result = scheduledFuture.get();
            System.out.println("result:"+result);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    /**
     * 使用Runnable进行一次任务调度
     */
    public static void scheduleByRunnable() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("execute task by Runnable...");
            }
        };

        scheduler.schedule(runnable,10, TimeUnit.SECONDS);
    }

    /**
     * 任务的间隔执行
     */
    public static void scheduleAtFixedRate() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("execute scheduleAtFixedRate task ...");
            }
        };

        // 延迟10s后触发，每隔5s执行一次
        scheduler.scheduleAtFixedRate(runnable, 10, 5, TimeUnit.SECONDS);
    }




}
