package cn.qing.executor.serial;

import java.util.concurrent.Executors;

/**
 * User: qing
 * Date: 2015-05-16 14:54
 * Desc:
 */
public class SerialExecutorTest {

    public static void main(String[] args) {
        SerialExecutor serialExecutor = new SerialExecutor(Executors.newSingleThreadExecutor());
        for (int i = 0; i < 30; i++) {
            serialExecutor.execute(new PrintRunnable("dingchangqign"+i));
        }
        for (int j = 0; j < 20; j++) {
            serialExecutor.execute(new PrintRunnable("zhangxiaojie"+j));
        }
    }
}
