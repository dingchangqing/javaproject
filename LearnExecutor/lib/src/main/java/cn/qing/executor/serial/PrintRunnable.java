package cn.qing.executor.serial;

/**
 * User: qing
 * Date: 2015-05-16 14:58
 * Desc: 用于打印输出的一个Runnable
 */
public class PrintRunnable implements Runnable {
    private String name = "";

    public PrintRunnable(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        System.out.println("name:"+name);
    }

    public String getName() {
        return name;
    }
}
